var express = require('express'),
  app = express(),
  port = process.env.PORT || 3000;

var path = require('path');

app.listen(port);

app.use(express.static(__dirname + "/build/default"));

console.log('Proyecto con wrapper de NodeJS ' + port);

app.get('/', function(req, res) {
  res.sendFile('index.html',{root:'.'});
})
